﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;
using DmrbTakip.Models.Interfaceses;
using DmrbTakip.Models.Contructers;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class DemirbasController : Controller
    {

        I_Helper hlp;
        I_Giris grs;
        DemirbasDBEntities db;
        public DemirbasController(I_Helper _hlp, I_Giris _grs, DemirbasDBEntities _db)
        {
            hlp = _hlp;
            grs = _grs;
            db = _db;
        }

        public async Task<JsonResult> GetDemirbasKayit(long serino)
        {
            var sonuc = await grs.DemirbasKayitBul(serino);
            if (sonuc == null)
            {
                sonuc = new Models.Contructers.DemirbasView();
            }
            return Json(sonuc, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Index()
        {
            var demirbas1 = db.Demirbas.Include(w => w.Model).Include(r => r.Model.Marka).Include(y => y.Model.Kategori);
            ViewBag.Helper = hlp;
            return View(await demirbas1.ToListAsync());
        }

        public async Task<ActionResult> Depo()                                     
        {
            List<int> zimmetliler =await db.Zimmets.Where(r => r.durum == 1).Select(r=>r.demirbas_id).ToListAsync();
            List<int> demirbas = await db.Demirbas.Where(e => e.durum == 1).Select(r => r.id).ToListAsync();
            List<int> fark = demirbas.Except(zimmetliler).ToList();
            var demirbas1 = db.Demirbas.Where(t => fark.Contains(t.id));
            ViewBag.Helper = hlp;
            return View(await demirbas1.ToListAsync());
        }

        public async Task<ActionResult> Kullanim()
        {
            var demirbas1 = db.Zimmets.Where(r=>r.durum==1);
            ViewBag.Helper = hlp;
            return View(await demirbas1.ToListAsync());
        }

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Demirba demirbas = await db.Demirbas.FindAsync(id);
            if (demirbas == null)
            {
                return HttpNotFound();
            }
            return View(demirbas);
        }


        public async Task<ActionResult> Create()
        {
            long serino = await db.Demirbas.OrderByDescending(r => r.serino).Select(r => r.serino).FirstOrDefaultAsync();
            serino = serino + 1;
            ViewBag.serino = serino;
            ViewBag.kategori_id = await grs.KategoriListe().ToListAsync();
            return View();
        }


        public async Task<JsonResult> getMarkas(int kategori_id)
        {
            IList<MarkaView> sonuc = await grs.Modeller().Where(e => e.kategori_id == kategori_id && e.Marka.durum == 1 && e.durum == 1).GroupBy(e=>new { e.marka_id, e.Marka.markaadi}).Select(r => new MarkaView
            {
                id = r.Key.marka_id,
                markaadi = r.Key.markaadi
            }).ToListAsync();

            return Json(sonuc, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getModels(int marka_id, int kategori_id)
        {
            IList<ModelView> data = await grs.Modeller().Where(t => t.marka_id == marka_id && t.kategori_id == kategori_id).Select(r => new ModelView { id = r.id, modeladi = r.modeladi + " (" + r.modelkodu + ")" }).ToListAsync();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DBKaydet(Demirba demirbas, HttpPostedFileBase image, string tarihd)
        {
            string sonuc = string.Empty;
            try
            {
                long serino = await db.Demirbas.OrderByDescending(r => r.serino).Select(r => r.serino).FirstOrDefaultAsync();
                serino = serino + 1;
                if (demirbas.serino != serino)
                {                   
                    demirbas.serino = serino;
                }
                string resimx = hlp.saveImageFile(image, "");
                demirbas.resim = resimx;
                DateTime tarihx = Convert.ToDateTime(tarihd);
                demirbas.alisTarihi = tarihx;
                db.Demirbas.Add(demirbas);
                int sonucx = await db.SaveChangesAsync();
                sonuc = sonucx == 1 ? "Demirbaş başarı ile kaydedildi" : "Kayıt sırasında hata meydana geldi.";
            }
            catch (Exception ex)
            {
                sonuc = ex.Message;
            }
            //ViewBag.kategori_id = await grs.KategoriListe().ToListAsync();  
            return Json(sonuc, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Demirba demirbas = await db.Demirbas.FindAsync(id);
            if (demirbas == null)
            {
                return HttpNotFound();
            }
            long serino = await db.Demirbas.OrderByDescending(r => r.serino).Select(r => r.serino).FirstOrDefaultAsync();
            serino = serino + 1;
            ViewBag.serino = serino;
            ViewBag.kategori_id = await grs.KategoriListe().ToListAsync();
            ViewBag.marka_id = await grs.MarkaListe().ToListAsync();
            return View(demirbas);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Demirba demirbas, HttpPostedFileBase image)
        {
            string sonuc = string.Empty;
            try
            {
                if (image != null)
                {
                    string resim = hlp.saveImageFile(image, demirbas.resim);
                    demirbas.resim = resim;
                }               
                db.Entry(demirbas).State = EntityState.Modified;
                int sonucx = await db.SaveChangesAsync();
                sonuc = sonucx == 1 ? "Demirbaş başarı ile Güncellendi" : "Güncelleme sırasında hata meydana geldi.";
            }
            catch (Exception ex)
            {
                sonuc = ex.Message;
            }

            //ViewBag.kategori_id = new SelectList(db.Kategoris, "id", "modeladi", demirbas.model_id);
            return Json(sonuc, JsonRequestBehavior.AllowGet);
        }

    }
}
