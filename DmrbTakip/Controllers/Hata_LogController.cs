﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;

namespace DmrbTakip.Controllers
{
    public class Hata_LogController : Controller
    {
        DemirbasDBEntities db;
        public Hata_LogController(DemirbasDBEntities _db)
        {
            db = _db;
        }

        // GET: Hata_Log
        public async Task<ActionResult> Index()
        {
            return View(await db.Hata_Log.ToListAsync());
        }


        // GET: a/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hata_Log hata_Log = await db.Hata_Log.FindAsync(id);
            if (hata_Log == null)
            {
                return HttpNotFound();
            }
            return View(hata_Log);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
