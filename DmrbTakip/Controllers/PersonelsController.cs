﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;
using DmrbTakip.Models.Interfaceses;
using DmrbTakip.Models.Contructers;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class PersonelsController : Controller
    {
        I_Helper hlp;
        I_Giris grs;
        DemirbasDBEntities db;
        public PersonelsController(I_Helper _hlp, I_Giris _grs, DemirbasDBEntities _db)
        {
            hlp = _hlp;
            grs = _grs;
            db = _db;
        }


        public async Task<ActionResult> Index()
        {
            var personels = db.Personels.Include(p => p.Birim);
            return View(await personels.ToListAsync());
        }


        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Personel personel = await db.Personels.FindAsync(id);
        //    if (personel == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(personel);
        //}


        public async Task<ActionResult> Create()
        {
            ViewBag.birim_id = await grs.BirimListe();
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Personel personel, HttpPostedFileBase image)
        {
            string sonuc = string.Empty;
            try
            {
                string resim = hlp.saveImageFile(image, "");
                personel.resim = resim;
                db.Personels.Add(personel);
                int sonucx = await db.SaveChangesAsync();
                sonuc = sonucx == 1 ? "Personel başarı ile kaydedildi" : "Kayıt sırasında hata meydana geldi.";
                return Json(sonuc, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                sonuc = ex.Message;
            }

            ViewBag.birim_id = new SelectList(db.Birims.Where(e => e.durum == 1), "id", "birimadi");
            return Json(sonuc, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Personel personel = await db.Personels.FindAsync(id);
            if (personel == null)
            {
                return HttpNotFound();
            }

            ViewBag.birim_id = new SelectList(await grs.BirimListe(), "id", "birimadi", personel.birim_id);

            return View(personel);
        }

        public async Task<JsonResult> GetBirim(int kat_id, int bina_id)
        {
            List<BirimView> data = await db.Birims.Where(e => e.durum == 1 && e.kat_id == kat_id && e.bina_id == bina_id).Select(r => new BirimView { id = r.id, birimadi = r.birimadi }).ToListAsync();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetKat()
        {
            List<KatView> data = await db.Kats.Where(e => e.durum == 1).Select(r => new KatView { id = r.id, katadi = r.katadi }).ToListAsync();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetBina()
        {
            List<BinaView> data = await db.Binas.Where(e => e.durum == 1).Select(r => new BinaView { id = r.id, binaadi = r.binaadi }).ToListAsync();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Personel personel, HttpPostedFileBase imagex)
        {
            string sonuc = string.Empty;
            if (ModelState.IsValid)
            {               
                try
                {
                    if (imagex != null)
                    {
                        string resimx = hlp.saveImageFile(imagex, personel.resim);
                        personel.resim = resimx;
                    }
                    db.Entry(personel).State = EntityState.Modified;
                    int sonucx = db.SaveChanges();
                    sonuc = sonucx == 1 ? "Personel başarı ile Güncellendi" : "Güncelleme sırasında hata meydana geldi.";
                }
                catch (Exception ex)
                {
                    sonuc = ex.Message;
                }           
            }         
            return Json(sonuc, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
