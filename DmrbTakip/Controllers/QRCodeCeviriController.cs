﻿using DmrbTakip.Models.Interfaceses;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web.Mvc;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class QRCodeCeviriController : Controller
    {
        private readonly I_Helper _helper;
        public QRCodeCeviriController(I_Helper helper)
        {
            _helper = helper;
        }

        public ActionResult BarcodeImage(string id)
        {
            return _helper.CreateQrCode(id);
        }


        //public ActionResult LabelPrint(string serino, string title, string detail)
        //{
        //    // Create the required label
        //    var label = new SharpPDFLabel.Labels.A4Labels.Avery.L7654();

        //    // Create a LabelCreator, passing the required label
        //    var labelCreator = new SharpPDFLabel.LabelCreator(label);

        //    //Add content to the labels

        //    //images
        //    FileStreamResult qrcode = _helper.CreateQrCode(serino);
        //    Stream outputStream = qrcode.FileStream;  
        //    labelCreator.AddImage(outputStream);

        //    //text
        //    labelCreator.IncludeLabelBorders = true;

        //    //labelCreator.AddText(title, "Verdana", 12, embedFont: true);
        //    //labelCreator.AddText("", "Verdana", 12, true, SharpPDFLabel.Enums.FontStyle.BOLD, SharpPDFLabel.Enums.FontStyle.UNDERLINE);


        //    //Create the PDF as a stream
        //    var pdfStream = labelCreator.CreatePDF();


        //    //Do something with it!
        //    Response.AddHeader("Content-Disposition", "attachment; filename=sheet_of_labels.pdf");
        //    return new FileStreamResult(pdfStream, "application/pdf");
        //}

        public ActionResult LabelPrint(string serino, string title, string detail)
        {

            Document doc = new Document(new iTextSharp.text.Rectangle(12, 6), 5, 5, 1, 1);

            try
            {

                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(
                  Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/codes.pdf", FileMode.Create));
                doc.Open();

                //System.Data.DataTable dt = new System.Data.DataTable();
                //dt.Columns.Add("ID");
                //dt.Columns.Add("Price");
                //for (int i = 0; i < 8; i++)
                //{
                //    System.Data.DataRow row = dt.NewRow();
                //    row["ID"] = "ZS00000000000000" + i.ToString();
                //    row["Price"] = "100," + i.ToString();
                //    dt.Rows.Add(row);
                //}

                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    if (i != 0)
                //        doc.NewPage();






                iTextSharp.text.pdf.PdfContentByte cb = writer.DirectContent;
                iTextSharp.text.pdf.BarcodeQRCode Qr = new BarcodeQRCode(serino, 60, 6, null);
                iTextSharp.text.Image img = Qr.GetImage();
                cb.SetTextMatrix(-2.0f, 0.0f);
                img.ScaleToFit(60, 5);
                img.SetAbsolutePosition(-2.8f, 0.5f);
                cb.AddImage(img);

                PdfContentByte cb1 = writer.DirectContent;
                BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_BOLDITALIC, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb1.SetFontAndSize(bf, 0.5f);
                cb1.BeginText();
                cb1.SetTextMatrix(0.2f, 5.1f);
                cb1.ShowText(detail);
                cb1.EndText();

                PdfContentByte id = writer.DirectContent;
                BaseFont bf1 = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                id.SetFontAndSize(bf1, 0.4f);
                id.BeginText();
                id.SetTextMatrix(0.2f, 0.6f);
                id.ShowText(title);
                id.EndText();
                //}
                // if you want to print it un comment the following two line

                //PdfAction act = new PdfAction(PdfAction.PRINTDIALOG);
                //writer.SetOpenAction(act);

                doc.Close();

                System.Diagnostics.Process.Start(Environment.GetFolderPath(
                           Environment.SpecialFolder.Desktop) + "/codes.pdf");

            }
            catch (Exception ex)
            {
            }
            finally
            {
                doc.Close();
            }
            return null;
        }

    }
}