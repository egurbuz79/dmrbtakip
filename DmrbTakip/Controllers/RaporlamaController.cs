﻿using DmrbTakip.DAL;
using DmrbTakip.Models.Contructers;
using DmrbTakip.Models.Interfaceses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class RaporlamaController : Controller
    {
        I_Helper hlp;
        DemirbasDBEntities db;
        public RaporlamaController(I_Helper _hlp, DemirbasDBEntities _db)
        {
            hlp = _hlp;
            db = _db;
        }
        // GET: Raporlama
        public JsonResult getPersonelData(int id)
        {
            PersonelZimmetView data = db.Personels.Where(e => e.id == id).Select(t => new PersonelZimmetView
            {
                ad = t.ad.ToUpper(),
                soyad = t.soyad.ToUpper(),
                birimadi = t.Birim.birimadi.ToUpper(),
                kat = t.Birim.Kat.katadi.ToUpper(),
                bina = t.Birim.Bina.binaadi.ToUpper(),
                adres = t.adres.ToUpper(),
                cinsiyet = t.cinsiyet == 2 ? "KADIN" : "ERKEK",
                dogumtarihi = t.dogumtarihi,
                eposta = t.eposta.ToLower(),
                resim = t.resim,
                telefon = t.telefon,
                tcno = t.tcno
            }).FirstOrDefault();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getOfisData(int id)
        {
            OfisZimmetView data = db.Ofis.Where(e => e.id == id).Select(t => new OfisZimmetView
            {

                ofisno = t.ofisno.ToUpper(),
                kat = t.Kat.katadi.ToUpper(),
                bina = t.Bina.binaadi.ToUpper(),
                eposta = t.eposta.ToLower(),
                telefon = t.telefon,
                firma = t.Ofis_Firma.Select(r => r.firmaadi).FirstOrDefault(),
                yetkili = t.yetkili

            }).FirstOrDefault();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPersonelZimmetData(int id)
        {
            List<ZimmetRapor> data = db.Zimmets.Where(r => r.personel_id == id).Select(w => new ZimmetRapor
            {
                sirano = w.Demirba.serino,
                demirbas = w.Demirba.demirbasadi,
                model = w.Demirba.Model.modeladi,
                marka = w.Demirba.Model.Marka.markaadi,
                kategori = w.Demirba.Model.Kategori.kategoriadi,
                aciklama = w.aciklama,
                zimmettarih = w.tarih,
                iadetarih = w.iadetarih
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getOfisZimmetData(int id)
        {
            List<ZimmetRapor> data = db.Zimmets.Where(r => r.ofis_id == id).Select(w => new ZimmetRapor
            {
                sirano = w.Demirba.serino,
                demirbas = w.Demirba.demirbasadi,
                model = w.Demirba.Model.modeladi,
                marka = w.Demirba.Model.Marka.markaadi,
                kategori = w.Demirba.Model.Kategori.kategoriadi,
                aciklama = w.aciklama,
                zimmettarih = w.tarih,
                iadetarih = w.iadetarih
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getDemirbasList()
        {
            List<DemirbasListView> liste = new List<DemirbasListView>();
            var data = db.Demirbas.ToList();
            for (int r = 0; r < data.Count; r++)
            {
                DemirbasListView kayit = new DemirbasListView();
                kayit.serino = data[r].serino;
                kayit.alistarihi = data[r].alisTarihi;
                kayit.demirbasadi = data[r].demirbasadi;
                string zimmetkontrol = data[r].Zimmets.Count == 0 ? "Depoda" : data[r].Zimmets.OrderByDescending(e => e.demirbas_id).Where(e => e.iadetarih == null || e.durum == 1).FirstOrDefault() == null ? "Depoda" : "Kullanımda";
                kayit.zimmet = zimmetkontrol;
                kayit.model = data[r].Model.modeladi;
                kayit.marka = data[r].Model.Marka.markaadi;
                kayit.kategori = data[r].Model.Kategori.kategoriadi;
                kayit.personel = "";
                kayit.birim = "";
                //resim = r.resim
                var personel = data[r].Zimmets.Select(e => new { e.Personel.ad, e.Personel.soyad, e.Personel.Birim.birimadi }).FirstOrDefault();
                if (personel != null)
                {
                    kayit.personel = personel.ad + " " + personel.soyad;
                    kayit.birim = personel.birimadi;
                }                
                liste.Add(kayit);
            }

            return Json(liste, JsonRequestBehavior.AllowGet);
        }



    }
}