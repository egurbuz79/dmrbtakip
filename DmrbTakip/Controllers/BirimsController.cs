﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class BirimsController : Controller
    {
        DemirbasDBEntities db;
        public BirimsController(DemirbasDBEntities _db)
        {
            db = _db;
        }

        public async Task<ActionResult> Index()
        {
            var birim = db.Birims.Include(o => o.Bina).Include(w => w.Kat);
            return View(await birim.ToListAsync());           
        }



        public async Task<ActionResult> Create()
        {
            ViewBag.bina_id = new SelectList(await db.Binas.Where(e => e.durum == 1 && e.yonetim == true).ToListAsync(), "id", "binaadi");
            ViewBag.kat_id = new SelectList(await db.Kats.Where(e => e.durum == 1).ToListAsync(), "id", "katadi");
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,birimadi,durum,bina_id,kat_id")] Birim birim)
        {
            if (ModelState.IsValid)
            {
                db.Birims.Add(birim);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.bina_id = new SelectList(await db.Binas.Where(e => e.durum == 1 && e.yonetim == true).ToListAsync(), "id", "binaadi", birim.bina_id);
            ViewBag.kat_id = new SelectList(await db.Kats.Where(e => e.durum == 1).ToListAsync(), "id", "katadi", birim.kat_id);
            return View(birim);
        }

       
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Birim birim = await db.Birims.FindAsync(id);
            if (birim == null)
            {
                return HttpNotFound();
            }
            ViewBag.bina_id = new SelectList(await db.Binas.Where(e => e.durum == 1 && e.yonetim == true).ToListAsync(), "id", "binaadi", birim.bina_id);
            ViewBag.kat_id = new SelectList(await db.Kats.Where(e => e.durum == 1).ToListAsync(), "id", "katadi", birim.kat_id);
            return View(birim);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,birimadi,durum,bina_id,kat_id")] Birim birim)
        {
            if (ModelState.IsValid)
            {
                db.Entry(birim).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.bina_id = new SelectList(await db.Binas.Where(e => e.durum == 1 && e.yonetim == true).ToListAsync(), "id", "binaadi", birim.bina_id);
            ViewBag.kat_id = new SelectList(await db.Kats.Where(e => e.durum == 1).ToListAsync(), "id", "katadi", birim.kat_id);
            return View(birim);
        }


    }
}
