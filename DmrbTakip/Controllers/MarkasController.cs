﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class MarkasController : Controller
    {
        DemirbasDBEntities db;
        public MarkasController(DemirbasDBEntities _db)
        {
            db = _db;
        }
        public async Task<ActionResult> Index()
        {
            return View(await db.Markas.ToListAsync());
        }

            
        
        public ActionResult Create()
        {
            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,markaadi,markakodu,durum")] Marka marka)
        {
            if (ModelState.IsValid)
            {
                db.Markas.Add(marka);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(marka);
        }

        
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Marka marka = await db.Markas.FindAsync(id);
            if (marka == null)
            {
                return HttpNotFound();
            }
            return View(marka);
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,markaadi,markakodu,durum")] Marka marka)
        {
            if (ModelState.IsValid)
            {
                db.Entry(marka).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(marka);
        }

       
      
       
    }
}
