﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class BinasController : Controller
    {
       
        DemirbasDBEntities db;
        public BinasController(DemirbasDBEntities _db)
        {           
            db = _db;
        }


        public async Task<ActionResult> Index()
        {
            return View(await db.Binas.ToListAsync());
        }
           
     
        public ActionResult Create()
        {
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,binaadi,durum")] Bina bina)
        {
            if (ModelState.IsValid)
            {
                db.Binas.Add(bina);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(bina);
        }

      
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bina bina = await db.Binas.FindAsync(id);
            if (bina == null)
            {
                return HttpNotFound();
            }
            return View(bina);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,binaadi,durum")] Bina bina)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bina).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(bina);
        }      

      
    }
}
