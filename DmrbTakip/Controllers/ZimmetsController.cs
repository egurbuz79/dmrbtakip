﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;
using DmrbTakip.Models.Contructers;
using DmrbTakip.Models.Interfaceses;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class ZimmetsController : Controller
    {
        DemirbasDBEntities db;
        I_Giris grs;
        public ZimmetsController(DemirbasDBEntities _db, I_Giris _grs)
        {
            db = _db;
            grs = _grs;
        }

        public async Task<ActionResult> PersonelListe()
        {

            try
            {
                ViewBag.Personeller = await db.Zimmets.Where(r => r.durum == 1).GroupBy(r => new PersonelZimmetView
                {
                    personel_id = r.personel_id.Value,
                    ad = r.Personel.ad,
                    soyad = r.Personel.soyad,
                    resim = r.Personel.resim,
                    birimadi = r.Personel.Birim.birimadi,
                    kat = r.Personel.Birim.Kat.katadi,
                    bina = r.Personel.Birim.Bina.binaadi,
                    durum =  r.Personel.durum
                }).Select(q => new PersonelZimmetView
                {
                    personel_id = q.Key.personel_id,
                    ad = q.Key.ad,
                    soyad = q.Key.soyad,
                    resim = q.Key.resim,
                    birimadi = q.Key.birimadi,
                    kat = q.Key.kat,
                    bina = q.Key.bina,
                    durum = q.Key.durum
                }).ToListAsync();
            }
            catch 
            {
                ViewBag.Personeller = new List<PersonelZimmetView>();
            }
            
            var zimmets = db.Zimmets.Include(z => z.Demirba).Include(z => z.Personel).Where(r => r.durum == 1 && r.ofis_id == null && r.personel_id != null);
            return View(await zimmets.ToListAsync());
        }

        public async Task<ActionResult> OfisListe()
        {
            try
            {
                ViewBag.Ofisler = await db.Zimmets.Where(r => r.durum == 1).GroupBy(r => new OfisZimmetView
                {
                    ofis_id = r.ofis_id.Value,
                    ofisno = r.Ofi.ofisno,
                    kat = r.Ofi.Kat.katadi,
                    bina = r.Ofi.Bina.binaadi
                    //eposta = r.Ofi.eposta,
                    //telefon = r.Ofi.telefon

                }).Select(q => new OfisZimmetView
                {
                    ofis_id = q.Key.ofis_id,
                    ofisno = q.Key.ofisno,
                    kat = q.Key.kat,
                    bina = q.Key.bina
                    //eposta = q.Key.eposta,
                    //telefon = q.Key.telefon
                }).ToListAsync();
            }
            catch 
            {
                ViewBag.Ofisler = new List<OfisZimmetView>();
            }
            
            var zimmets = db.Zimmets.Include(z => z.Demirba).Include(z => z.Ofi).Where(r => r.durum == 1 && r.ofis_id != null && r.personel_id == null);
            return View(await zimmets.ToListAsync());
        }

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zimmet zimmet = await db.Zimmets.FindAsync(id);
            if (zimmet == null)
            {
                return HttpNotFound();
            }
            return View(zimmet);
        }


        public async Task<ActionResult> Create()
        {
            ViewBag.kategori_id = await grs.KategoriListe().ToListAsync();
            ViewBag.birim_id = await grs.BirimListe();
            ViewBag.kat_id = await grs.KatListe();
            ViewBag.bina_id = await grs.BinaListe(true);
            return View();
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var zimmet = await db.Zimmets.Where(r => r.ofis_id == null && r.id == id.Value).FirstOrDefaultAsync();
            if (zimmet == null)
            {
                return HttpNotFound();
            }
            ViewBag.kategori_id = await grs.KategoriListe().ToListAsync();
            ViewBag.birim_id = await grs.BirimListe();
            ViewBag.kat_id = await grs.KatListe();
            ViewBag.bina_id = await grs.BinaListe(true);
            return View(zimmet);
        }

        public async Task<ActionResult> Ofis_Create()
        {
            ViewBag.kategori_id = await grs.KategoriListe().ToListAsync();
            ViewBag.kat_id = await grs.KatListe();
            ViewBag.bina_id = await grs.BinaListe(null);
            return View();
        }

        public async Task<ActionResult> Ofis_Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zimmet zimmet = await db.Zimmets.Where(r => r.personel_id == null && r.id == id.Value).FirstOrDefaultAsync();
            if (zimmet == null)
            {
                return HttpNotFound();
            }
            ViewBag.kategori_id = await grs.KategoriListe().ToListAsync();
            ViewBag.ofis_id = await grs.OfisKatListe(zimmet.Ofi.bina_id, zimmet.Ofi.kat_id);
            ViewBag.kat_id = await grs.KatListe();
            ViewBag.bina_id = await grs.BinaListe(null);
            return View(zimmet);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Zimmet zimmet)
        {
            string sonuc = string.Empty;
            try
            {
                db.Zimmets.Add(zimmet);
                int sonucx = await db.SaveChangesAsync();
                string durum = string.Empty;
                if (zimmet.personel_id == null)
                {
                    durum = "ofise";
                }
                else
                {
                    durum = "personele";
                }
                sonuc = sonucx == 1 ? "Zimmet seçili " + durum + " başarı ile yapıldı" : "Kayıt sırasında hata meydana geldi.";
                return Json(sonuc, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                sonuc = ex.Message;
                return Json(sonuc, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Zimmetiade(int id)
        {           
            var zimmet = db.Zimmets.Where(r => r.ofis_id == null && r.id == id).FirstOrDefault();
            zimmet.iadetarih = DateTime.Now;
            zimmet.durum = 0;
            int sonuc = db.SaveChanges();
            return Json(sonuc, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Zimmet zimmet, string tarihd, string iadetarihd)
        {
            string sonuc = string.Empty;
            zimmet.tarih = Convert.ToDateTime(tarihd);
            if (!string.IsNullOrEmpty(iadetarihd))
            {
                zimmet.iadetarih = Convert.ToDateTime(iadetarihd);
            }
           
            if (zimmet.durum == (int)Models.Enums.EnumHelp.Durum.pasif)
            {
                zimmet.iadetarih = DateTime.Now;
            }
           
            db.Entry(zimmet).State = EntityState.Modified;
            int sonucx = await db.SaveChangesAsync();
            sonuc = sonucx == 1 ? "Zimmet güncelleme işlemi başarı ile yapıldı" : "Güncelleme sırasında hata meydana geldi.";
            if (sonucx == 1)
            {
                return Json(sonuc, JsonRequestBehavior.AllowGet);
            }
            
            ViewBag.kategori_id = await grs.KategoriListe().ToListAsync();
            ViewBag.birim_id = await grs.BirimListe();
            ViewBag.kat_id = await grs.KatListe();
            ViewBag.bina_id = await grs.BinaListe(true);
            ViewBag.model_id = await grs.ModelListe().ToListAsync();
            return View(zimmet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Ofis_Edit(Zimmet zimmet)
        {
            string sonuc = string.Empty;
            if (zimmet.durum == (int)Models.Enums.EnumHelp.Durum.pasif)
            {
                zimmet.iadetarih = DateTime.Now;
            }
            db.Entry(zimmet).State = EntityState.Modified;
            int sonucx = await db.SaveChangesAsync();
            sonuc = sonucx == 1 ? "Zimmet güncelleme işlemi başarı ile yapıldı" : "Güncelleme sırasında hata meydana geldi.";
            if (sonucx == 1)
            {
                return Json(sonuc, JsonRequestBehavior.AllowGet);
            }

            ViewBag.kategori_id = await grs.KategoriListe().ToListAsync();
            ViewBag.birim_id = await grs.BirimListe();
            ViewBag.kat_id = await grs.KatListe();
            ViewBag.bina_id = await grs.BinaListe(true);
            ViewBag.model_id = await grs.ModelListe().ToListAsync();
            return View(zimmet);
        }

        public async Task<IList<DemirbasView>> ZimmetKontrol(int model_id)
        {
            var kontrol = await db.Demirbas.Where(e => e.durum == 1 && e.model_id == model_id && e.Zimmets.Where(q => q.iadetarih == null).Count() == 0).Select(w => new DemirbasView { id = w.id, demirbasAdi = w.demirbasadi, serino = w.serino }).ToListAsync();
            return kontrol;
        }

        public async Task<JsonResult> GetPersonels(int birim_id)
        {
            var data = db.Personels.Where(e => e.durum == 1 && e.birim_id == birim_id).Select(w => new PersonelListView { id = w.id, Adi = w.ad, Soyadi = w.soyad });
            return Json(await data.ToListAsync(), JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetOfis(int bina_id, int kat_id)
        {
            var ofisler = await grs.OfisKatListe(bina_id, kat_id);           
            return Json(ofisler, JsonRequestBehavior.AllowGet);
        }
        

        public async Task<JsonResult> GetBir(int birim_id)
        {
            var data = db.Personels.Where(e => e.durum == 1 && e.birim_id == birim_id).Select(w => new PersonelListView { id = w.id, Adi = w.ad, Soyadi = w.soyad });
            return Json(await data.ToListAsync(), JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetDemirbaslar(int model_id)
        {
            var data = await ZimmetKontrol(model_id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getMarkas(int kategori_id)
        {
            IList<MarkaView> sonuc = await grs.Modeller().Where(e => e.kategori_id == kategori_id && e.Marka.durum == 1 && e.durum == 1).Select(r => new MarkaView
            {
                id = r.marka_id,
                markaadi = r.Marka.markaadi
            }).ToListAsync();

            return Json(sonuc, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getModels(int marka_id, int kategori_id)
        {
            IList<ModelView> data = await grs.Modeller().Where(t => t.marka_id == marka_id && t.kategori_id == kategori_id).Select(r => new ModelView { id = r.id, modeladi = r.modeladi + " (" + r.modelkodu + ")" }).ToListAsync();
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public ActionResult QrReder()
        {
            return View();
        }

        public JsonResult GetQrReder(long reader)
        {
            ZimmetQr getirZimmet = db.Zimmets.OrderByDescending(w => w.id).Where(r => r.Demirba.serino == reader && r.durum==1 && r.Demirba.durum==1).Select(t => new
                ZimmetQr
            {
                id = t.id,
                ad = t.Personel.ad,
                soyad = t.Personel.soyad,
                kat = t.Personel.Birim.Kat.katadi ?? t.Ofi.Kat.katadi,
                bina = t.Personel.Birim.Bina.binaadi ?? t.Ofi.Bina.binaadi,
                birim = t.Personel.Birim.birimadi,
                dbadi = t.Demirba.demirbasadi,
                dbresim = t.Demirba.resim,
                resim = t.Personel.resim,
                dbseri = t.Demirba.serino,
                ofisno = t.Ofi.ofisno,
                yetkili = t.Ofi.yetkili,
                dbmodel = t.Demirba.Model.modeladi,
                dbmarka = t.Demirba.Model.Marka.markaadi,
                dbkategori = t.Demirba.Model.Kategori.kategoriadi
            }).FirstOrDefault();
            if (getirZimmet == null)
            {
                getirZimmet = new ZimmetQr();
            }
            return Json(getirZimmet, JsonRequestBehavior.AllowGet);
        }


    }
}
