﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class KatsController : Controller
    {
        DemirbasDBEntities db;
        public KatsController(DemirbasDBEntities _db)
        {
            db = _db;
        }

        public async Task<ActionResult> Index()
        {
            return View(await db.Kats.ToListAsync());
        }

       
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kat kat = await db.Kats.FindAsync(id);
            if (kat == null)
            {
                return HttpNotFound();
            }
            return View(kat);
        }

       
        public ActionResult Create()
        {
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,katadi,durum")] Kat kat)
        {
            if (ModelState.IsValid)
            {
                db.Kats.Add(kat);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(kat);
        }

       
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kat kat = await db.Kats.FindAsync(id);
            if (kat == null)
            {
                return HttpNotFound();
            }
            return View(kat);
        }

        // POST: Kats/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,katadi,durum")] Kat kat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kat).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(kat);
        }

       
        
    }
}
