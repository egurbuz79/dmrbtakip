﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class Islem_LogController : Controller
    {
        DemirbasDBEntities db;
        public Islem_LogController(DemirbasDBEntities _db)
        {
            db = _db;
        }

        // GET: Islem_Log
        public async Task<ActionResult> Index()
        {
            var islem_Log = db.Islem_Log.Include(i => i.Personel);
            return View(await islem_Log.ToListAsync());
        }

        // GET: Islem_Log/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Islem_Log islem_Log = await db.Islem_Log.FindAsync(id);
            if (islem_Log == null)
            {
                return HttpNotFound();
            }
            return View(islem_Log);
        }

       
    }
}
