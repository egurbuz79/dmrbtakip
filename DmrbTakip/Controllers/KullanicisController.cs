﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class KullanicisController : Controller
    {
        DemirbasDBEntities db;
        public KullanicisController(DemirbasDBEntities _db)
        {
            db = _db;
        }
        public async Task<ActionResult> Index()
        {
            var kullanicis = db.Kullanicis.Include(k => k.Rol);
            return View(await kullanicis.ToListAsync());
        }

     
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici kullanici = await db.Kullanicis.FindAsync(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }

       
        public ActionResult Create()
        {
            ViewBag.rol_id = new SelectList(db.Rols.Where(w => w.durum == 1), "id", "rol1");
            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,kullaniciAdi,sifre,tarih,durum,rol_id")] Kullanici kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Kullanicis.Add(kullanici);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.rol_id = new SelectList(db.Rols.Where(w => w.durum == 1), "id", "rol1", kullanici.rol_id);
            return View(kullanici);
        }

       
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici kullanici = await db.Kullanicis.FindAsync(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            ViewBag.rol_id = new SelectList(db.Rols.Where(w=>w.durum == 1), "id", "rol1", kullanici.rol_id);
            return View(kullanici);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,kullaniciAdi,sifre,tarih,durum,rol_id")] Kullanici kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kullanici).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.rol_id = new SelectList(db.Rols.Where(w => w.durum == 1), "id", "rol1", kullanici.rol_id);
            return View(kullanici);
        }

        
    }
}
