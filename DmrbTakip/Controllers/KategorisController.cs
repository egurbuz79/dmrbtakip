﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class KategorisController : Controller
    {
        DemirbasDBEntities db;
        public KategorisController(DemirbasDBEntities _db)
        {
            db = _db;
        }

        public async Task<ActionResult> Index()
        {
            return View(await db.Kategoris.ToListAsync());
        }


        public ActionResult Create()
        {
            return View();
        }

     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Kategori kategori)
        {
            if (ModelState.IsValid)
            {
                db.Kategoris.Add(kategori);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(kategori);
        }

      
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kategori kategori = await db.Kategoris.FindAsync(id);
            if (kategori == null)
            {
                return HttpNotFound();
            }
            return View(kategori);
        }

      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,kategoriAdi,durum")] Kategori kategori)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kategori).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(kategori);
        }
        
    }
}
