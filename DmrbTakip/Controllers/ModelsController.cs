﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;
using DmrbTakip.Models.Interfaceses;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class ModelsController : Controller
    {
        I_Giris grs;
        DemirbasDBEntities db;
        public ModelsController(I_Giris _grs, DemirbasDBEntities _db)
        {
            grs = _grs;
            db = _db;
        }

        public async Task<ActionResult> Index()
        {
            var models = db.Models.Include(m => m.Kategori).Include(m => m.Marka);
            return View(await models.ToListAsync());
        }
               
        public async Task<ActionResult> Create()
        {
            ViewBag.kategori_id = new SelectList(await grs.KategoriListe().ToListAsync(), "id", "kategoriadi");
            ViewBag.marka_id = new SelectList(await grs.MarkaListe().ToListAsync(), "id", "markaadi");
            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,modeladi,modelkodu,durum,kategori_id,marka_id")] Model model)
        {
            if (ModelState.IsValid)
            {
                db.Models.Add(model);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.kategori_id = new SelectList(await grs.KategoriListe().ToListAsync(), "id", "kategoriadi");
            ViewBag.marka_id = new SelectList(await grs.MarkaListe().ToListAsync(), "id", "markaadi");
            return View(model);
        }

        
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Model model = await db.Models.FindAsync(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            ViewBag.kategori_id = new SelectList(db.Kategoris, "id", "kategoriadi", model.kategori_id);
            ViewBag.marka_id = new SelectList(db.Markas, "id", "markaadi", model.marka_id);
            return View(model);
        }

      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,modeladi,modelkodu,durum,kategori_id,marka_id")] Model model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.kategori_id = new SelectList(db.Kategoris, "id", "kategoriadi", model.kategori_id);
            ViewBag.marka_id = new SelectList(db.Markas, "id", "markaadi", model.marka_id);
            return View(model);
        }

       
      

        
    }
}
