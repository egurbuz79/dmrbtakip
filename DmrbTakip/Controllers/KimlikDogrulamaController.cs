﻿using DmrbTakip.DAL;
using DmrbTakip.Models.Contructers;
using DmrbTakip.Models.Methods;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace DmrbTakip.Controllers
{

    public class KimlikDogrulamaController : Controller
    {
        DemirbasDBEntities db;
        public KimlikDogrulamaController(DemirbasDBEntities _db)
        {
            db = _db;
        }

        public ActionResult GirisYap()
        {
            return View();

        }

        public ActionResult GirisBasarili(DAL.Kullanici uye, bool hatirla)
        {
            Result result = new Result();
            var kontrol = db.Kullanicis.Where(e => e.kullaniciadi == uye.kullaniciadi && e.sifre == uye.sifre).FirstOrDefault();
            if (kontrol != null) //Check the database
            {
                int userid = kontrol.id;
                List<Claim> claims = new Autorizing(db).GetClaims(kontrol); //Get the claims from the headers or db or your user store
                if (null != claims)
                {
                    new Autorizing(db).SignIn(claims, hatirla);
                    var identity = (ClaimsIdentity)User.Identity;
                    List<string> rol = identity.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).ToList();
                    result.adres = "../Giris/Index";
                    result.sonuc = true;
                }
            }
            else
            {
                result.sonuc = false;
                result.mesaj = "Hatalı kullanıcı adı ve/vaya şifre";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult KayitOl()
        {
            return View();
        }

        public ActionResult SifreKurtar()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult LogOff()
        {
            new Autorizing(db).LogOff();           
            Response.Cookies.Clear();
            FormsAuthentication.SignOut();
            HttpCookie c = new HttpCookie("Login");
            c.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(c);
            Session.Clear();
            return RedirectToAction("GirisYap", "KimlikDogrulama", new { area = "" });
        }

        //public IAuthenticationManager AuthenticationManager
        //{
        //    get
        //    {
        //        return HttpContext.GetOwinContext().Authentication;
        //    }
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult KayitOnay(FormCollection data)
        //{
        //    string sonuc = string.Empty;
        //    try
        //    {
        //        using (DAL.DemirbasDBEntities db = new DAL.DemirbasDBEntities())
        //        {
        //            var bilgi = new DAL.Kullanici();
        //            bilgi.kullaniciAdi = data["kullaniciAdi"];
        //            bilgi.sifre = data["sifre"];
        //            bilgi.durum = 1;
        //            bilgi.tarih = DateTime.Now.Date;
        //            bilgi.rol_id = 2; //üye id
        //            db.Kullanicis.Add(bilgi);
        //            db.SaveChanges();
        //            sonuc = "İşlem Başarılı";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sonuc = ex.Message;
        //    }
        //    return Json(sonuc, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult KilitEkranı()
        {
            return View();
        }
    }
}