﻿using DmrbTakip.Models.Interfaceses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;
using DmrbTakip.Models.Contructers;
using System.Data.Entity;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class GirisController : Controller
    {
        private I_Helper _hlp;
        private I_Giris _grs;
        public GirisController(I_Helper hlp, I_Giris grs)
        {
            _hlp = hlp;
            _grs = grs;
        }

        // GET: Giris
        public async Task<ActionResult> Index()
        {   
            Tuple <int,int,int,int,int> sayilar = await _grs.GetCounts();
            int demirbasSayisi = sayilar.Item1;
            int zimmetSayisi = sayilar.Item2;
            int depoSayisi = sayilar.Item3;
            int islemlogsayi = sayilar.Item4;
            int hatalogsayi = sayilar.Item5;

            IList<Demirba> demirbasListe = await _grs.DemirbasListe().ToListAsync();
            IList<Model> modelListe = await _grs.ModelListe().ToListAsync();
            IList<Marka> markaListe = await _grs.MarkaListe().ToListAsync();
            IList<Kategori> kategoriListe = await _grs.KategoriListe().ToListAsync();


            IList<Zimmet> sonYapilanZimmetler = await _grs.SonYapilanZimmetler();
            IList<Islem_Log> sonAlinanIslemHatalar = await _grs.SonAlinanIslemLoglari();

            ViewBag.demirbasSayisi = demirbasSayisi;
            ViewBag.zimmetSayisi = zimmetSayisi;
            ViewBag.depoSayisi = depoSayisi;
            ViewBag.islemlogsayi = islemlogsayi;
            ViewBag.hatalogsayi = hatalogsayi;

            ViewData["demirbasListe"] = demirbasListe;
            ViewData["modelListe"] = modelListe;
            ViewData["markaListe"] = markaListe;
            ViewData["kategoriListe"] = kategoriListe;
            ViewData["sonYapilanZimmetler"] = sonYapilanZimmetler;
            ViewData["sonAlinanIslemHatalar"] = sonAlinanIslemHatalar;

            IList<DemirbasKullanimOranView> gelenORan = await _grs.ZimmetOranlama();
            ViewBag.Oran = gelenORan;

            Tuple<int, int> demirbasOran =await _grs.KullanimDemirbasOrani();
            ViewBag.DemirbasOran = demirbasOran;

            return View();
        }  
      
    }
}