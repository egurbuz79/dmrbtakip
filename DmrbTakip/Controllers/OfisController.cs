﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class OfisController : Controller
    {
        DemirbasDBEntities db;
        public OfisController(DemirbasDBEntities _db)
        {
            db = _db;
        }

        public async Task<ActionResult> Index()
        {
            var ofis = db.Ofis.Include(o => o.Bina).Include(w => w.Kat);
            return View(await ofis.ToListAsync());
        }


        public async Task<ActionResult> Create()
        {
            ViewBag.bina_id = new SelectList(await db.Binas.Where(e => e.durum == 1 && e.yonetim != true).ToListAsync(), "id", "binaadi");
            ViewBag.kat_id = new SelectList(await db.Kats.Where(e => e.durum == 1).ToListAsync(), "id", "katadi");
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Ofi ofi)
        {
            ofi.yetkili = "Admin";
            db.Ofis.Add(ofi);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }


        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ofi ofi = await db.Ofis.FindAsync(id);
            if (ofi == null)
            {
                return HttpNotFound();
            }
            ViewBag.bina_id = new SelectList(await db.Binas.Where(e => e.durum == 1 && e.yonetim != true).ToListAsync(), "id", "binaadi", ofi.bina_id);
            ViewBag.kat_id = new SelectList(await db.Kats.Where(e => e.durum == 1).ToListAsync(), "id", "katadi", ofi.kat_id);
            return View(ofi);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Ofi ofi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ofi).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.bina_id = new SelectList(await db.Binas.Where(e => e.durum == 1 && e.yonetim != true).ToListAsync(), "id", "binaadi", ofi.bina_id);
            ViewBag.kat_id = new SelectList(await db.Kats.Where(e => e.durum == 1).ToListAsync(), "id", "katadi", ofi.kat_id);
            return View(ofi);
        }


    }
}
