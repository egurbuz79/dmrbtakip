﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DmrbTakip.DAL;

namespace DmrbTakip.Controllers
{
    [Authorize]
    public class RolsController : Controller
    {
        DemirbasDBEntities db;
        public RolsController(DemirbasDBEntities _db)
        {
            db = _db;
        }

        public async Task<ActionResult> Index()
        {
            return View(await db.Rols.ToListAsync());
        }

       
       
        public ActionResult Create()
        {
            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,rol1,durum")] Rol rol)
        {
            if (ModelState.IsValid)
            {
                db.Rols.Add(rol);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(rol);
        }

       
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rol rol = await db.Rols.FindAsync(id);
            if (rol == null)
            {
                return HttpNotFound();
            }
            return View(rol);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,rol1,durum")] Rol rol)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rol).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(rol);
        }

       

        
    }
}
