﻿using DmrbTakip.DAL;
using DmrbTakip.Models.Interfaceses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using DmrbTakip.Models.Contructers;
using DmrbTakip.Models.Enums;

namespace DmrbTakip.Models.Methods
{
    public class Giris : I_Giris
    {
        private DemirbasDBEntities _db;
        public Giris(DemirbasDBEntities db)
        {
            _db = db;
        }

        public IQueryable<Demirba> DemirbasListe()
        {
            var liste = _db.Demirbas.OrderByDescending(w => w.id).Take(10);
            return liste;
        }


        public IQueryable<Marka> MarkaListe()
        {
            var liste = _db.Markas.OrderByDescending(w => w.markaadi).Where(r => r.durum == 1);
            return liste;
        }

        /// <summary>
        /// Modeller DB den çekilen tüm data
        /// </summary>
        /// <returns></returns>
        public IQueryable<Model> Modeller()
        {
            IQueryable<Model> liste = _db.Models.OrderByDescending(w => w.modeladi).Where(r => r.durum == 1);
            return liste;
        }

        public IQueryable<Kategori> KategoriListe()
        {
            var liste = _db.Kategoris.OrderByDescending(w => w.kategoriadi).Where(r => r.durum == 1);
            return liste;
        }

        /// <summary>
        /// Zimmet ataması yapılacakken depoda olan modeller
        /// </summary>
        /// <returns></returns>
        public IQueryable<Model> ModelListe()
        {
            IQueryable<Model> data = _db.Models.Where(e => e.durum == 1);
            var kontrolx = data.Where(e => e.Demirbas.Any(u => (u.Zimmets.Count == 0 || u.Zimmets.Any(q => q.iadetarih != null))));
            return kontrolx;
        }


        public async Task<IList<Kat>> KatListe()
        {
            IQueryable<Kat> data = _db.Kats.Where(e => e.durum == 1);
            return await data.ToListAsync();
        }

        public async Task<IList<Bina>> BinaListe(bool? yonetim)
        {
            IQueryable<Bina> data = _db.Binas.Where(e => e.durum == 1);
            if (yonetim != null && yonetim==true)
            {
                data = data.Where(r => r.yonetim == yonetim);
            }
            else
            {
                data = data.Where(r => r.yonetim == null);
            }
            return await data.ToListAsync();
        }

        public async Task<DemirbasView> DemirbasKayitBul(long serino)
        {
            var kayit = await _db.Demirbas.Where(w => w.serino == serino).Select(q => new DemirbasView
            {
                id = q.id,
                demirbasAdi = q.demirbasadi
            }).FirstOrDefaultAsync();
            return kayit;
        }

        public async Task<IList<Islem_Log>> SonAlinanIslemLoglari()
        {
            var liste = await _db.Islem_Log.OrderByDescending(w => w.id).Take(10).ToListAsync();
            return liste;
        }

        public async Task<IList<Zimmet>> SonYapilanZimmetler()
        {
            var liste = await _db.Zimmets.OrderByDescending(w => w.id).Where(r=>r.durum==1).Take(10).ToListAsync();
            return liste;
        }

        public async Task<Tuple<int, int, int, int, int>> GetCounts()
        {
            List<int> zimmetliler = await _db.Zimmets.Where(r => r.durum == 1).Select(r => r.demirbas_id).ToListAsync();
            List<int> demirbas = await _db.Demirbas.Where(e => e.durum == 1).Select(r => r.id).ToListAsync();
            List<int> fark = demirbas.Except(zimmetliler).ToList();

            int demirbasSayi = demirbas.Count;
            int zimmetSayisi = zimmetliler.Count;
            int depoSayisi = fark.Count;
            int islemLogSayisi = await _db.Islem_Log.CountAsync();
            int hataLogSayisi = await _db.Hata_Log.CountAsync();

            var degerler = new Tuple<int, int, int, int, int>(demirbasSayi, zimmetSayisi, depoSayisi, islemLogSayisi, hataLogSayisi);
            return degerler;

        }

        public async Task<IList<DemirbasKullanimOranView>> ZimmetOranlama()
        {

            var toplamSayilar = await (from x in _db.Demirbas
                                       where x.Zimmets.Where(e => e.iadetarih == null).Count() > 0
                                       group x by x.Model.Kategori.kategoriadi into g
                                       orderby g.Key
                                       select new
                                       {
                                           KategoriAdi = g.Key,
                                           Adet = g.Select(x => x.demirbasadi).Distinct().Count()
                                       }).ToListAsync();

            var toplamSayilarGenel = await (from x in _db.Demirbas
                                            group x by x.Model.Kategori.kategoriadi into g
                                            orderby g.Key
                                            select new
                                            {
                                                KategoriAdi = g.Key,
                                                Adet = g.Select(x => x.id).Count()
                                            }).ToListAsync();
            List<DemirbasKullanimOranView> data = new List<DemirbasKullanimOranView>();
            for (int ix = 0; ix < toplamSayilar.Count; ix++)
            {
                DemirbasKullanimOranView dd = new DemirbasKullanimOranView();
                dd.DemirbasKAtegori = toplamSayilar[ix].KategoriAdi;
                dd.KullanilanAdet = toplamSayilar[ix].Adet;
                dd.ToplamAdet = toplamSayilarGenel[ix].Adet;
                dd.Oran = ((dd.KullanilanAdet / dd.ToplamAdet) * 100);
                data.Add(dd);
            }
            return data;
        }

        public async Task<Tuple<int, int>> KullanimDemirbasOrani()
        {
            IQueryable<Demirba> deger = _db.Demirbas.Where(q => q.durum == (int)EnumHelp.Durum.aktif);
            int kullanilan = await deger.Where(e => e.Zimmets.Any(r => r.iadetarih == null)).CountAsync();
            int depoda = await deger.Where(e => e.Zimmets.Any(r => r.iadetarih != null || r.durum == 0)).CountAsync();
            Tuple<int, int> sonuc = new Tuple<int, int>(kullanilan, depoda);
            return sonuc;
        }

        public async Task<IList<BirimView>> BirimListe()
        {
            var liste = await _db.Birims.OrderByDescending(w => w.birimadi).Where(r => r.durum == 1).Select(o => new BirimView
            {
                id = o.id,
                birimadi = o.birimadi
            }).ToListAsync();
            return liste;
        }

        public async Task<IList<OfisView>> OfisKatListe(int binaId, int katId)
        {
            var liste = await _db.Ofis.OrderByDescending(w => w.ofisno).Where(r => r.durum == 1 && r.bina_id==binaId && r.kat_id==katId).Select(o => new OfisView
            {
                id = o.id,
                ofisno = o.ofisno
            }).ToListAsync();
            return liste;
        }
    }
}