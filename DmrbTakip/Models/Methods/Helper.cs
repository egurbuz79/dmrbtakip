﻿using DmrbTakip.Models.Interfaceses;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace DmrbTakip.Models.Methods
{
    public class Helper : I_Helper
    {
        public void HataLogYaz(string baslik, string icerik)
        {
            throw new NotImplementedException();
        }

        public void IslemLogYaz(string baslik, string icerik, int personel_id)
        {
            throw new NotImplementedException();
        }

        public string saveImageFile(HttpPostedFileBase document, string oldimg)
        {
            if (document != null)
            {
                if (oldimg != "")
                {
                    removeImage(oldimg);
                }
                string yolKontrol = HttpContext.Current.Server.MapPath("~/Resimler/");
                if (!Directory.Exists(yolKontrol))
                {
                    Directory.CreateDirectory(yolKontrol);
                }
                FileInfo f = new FileInfo(document.FileName);
                string Fname = Guid.NewGuid().ToString().Split('-')[0] + f.Name;                
                document.SaveAs(yolKontrol + Fname);
                return Fname;
            }
            else
            {
                return "";
            }
        }

        private void removeImage(string oldimg)
        {
            var silDosya = HttpContext.Current.Server.MapPath("~/Resimler/" + oldimg);
            if (File.Exists(silDosya))
            {
                File.Delete(silDosya);
            }
        }

        public FileStreamResult CreateQrCode(string text)
        {
            // generating a barcode here. Code is taken from QrCode.Net library
            QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = new QrCode();
            qrEncoder.TryEncode(text, out qrCode);
            GraphicsRenderer renderer = new GraphicsRenderer(new FixedModuleSize(4, QuietZoneModules.Four), Brushes.Black, Brushes.White);

            Stream memoryStream = new MemoryStream();
            renderer.WriteToStream(qrCode.Matrix, ImageFormat.Png, memoryStream);

            // very important to reset memory stream to a starting position, otherwise you would get 0 bytes returned
            memoryStream.Position = 0;

            var resultStream = new FileStreamResult(memoryStream, "image/png");
            resultStream.FileDownloadName = String.Format("{0}.png", text);

            return resultStream;
        }


    }
}