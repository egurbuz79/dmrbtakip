﻿using DmrbTakip.DAL;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace DmrbTakip.Models.Methods
{
    public class Autorizing
    {
        private DemirbasDBEntities db;
        public Autorizing(DemirbasDBEntities _db)
        {
            db = _db;
        }

        public List<Claim> GetClaims(Kullanici data)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.NameIdentifier, data.kullaniciadi));
            claims.Add(new Claim(ClaimTypes.Sid, data.id.ToString()));
            claims.Add(new Claim(ClaimTypes.Name, data.kullaniciadi));
            string resim = "/Resimler/resimyok.jpg";
            claims.Add(new Claim(ClaimTypes.Upn, resim));
            claims.Add(new Claim(ClaimTypes.Role, data.Rol.rol1));
            return claims;
        }


        public void SignIn(List<Claim> claims, bool hatirla)
        {
            var claimsIdentity = new ClaimsIdentity(claims,
            DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = hatirla }, claimsIdentity);
            HttpContext.Current.User = new ClaimsPrincipal(AuthenticationManager.AuthenticationResponseGrant.Principal);
        }

        public void LogOff()
        {
            _authnManager = HttpContext.Current.GetOwinContext().Authentication;
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        public IAuthenticationManager _authnManager;

        public IAuthenticationManager AuthenticationManager
        {
            get
            {
                if (_authnManager == null)
                    _authnManager = HttpContext.Current.GetOwinContext().Authentication;
                return _authnManager;
            }
            set
            {
                _authnManager = value;
            }
        }

    }
}
