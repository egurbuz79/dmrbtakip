﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DmrbTakip.Models.Interfaceses
{
    public interface I_Helper
    {
        void IslemLogYaz(string baslik, string icerik, int personel_id);
        void HataLogYaz(string baslik, string icerik);
        string saveImageFile(HttpPostedFileBase document, string oldimg);
        FileStreamResult CreateQrCode(string text);
    }
}
