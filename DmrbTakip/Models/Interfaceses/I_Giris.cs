﻿using DmrbTakip.DAL;
using DmrbTakip.Models.Contructers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DmrbTakip.Models.Interfaceses
{
    public interface I_Giris
    {
        IQueryable<Demirba> DemirbasListe();
        IQueryable<Model> ModelListe();
        IQueryable<Marka> MarkaListe();
        IQueryable<Model> Modeller();
        IQueryable<Kategori> KategoriListe();
        Task<DemirbasView> DemirbasKayitBul(long serino);
        Task<IList<Zimmet>> SonYapilanZimmetler();
        Task<IList<Islem_Log>> SonAlinanIslemLoglari();
        Task<Tuple<int, int, int, int, int>> GetCounts();
        Task<IList<DemirbasKullanimOranView>> ZimmetOranlama();
        Task<Tuple<int, int>> KullanimDemirbasOrani();          
        Task<IList<Bina>> BinaListe(bool? yonetim);
        Task<IList<Kat>> KatListe();
        Task<IList<BirimView>> BirimListe();
        Task<IList<OfisView>> OfisKatListe(int binaId, int katId);
    }
}
