﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DmrbTakip.Models.Contructers
{
    public class ZimmetQr
    {
        public int id { get; set; }
        public string kat { get; set; }
        public string bina { get; set; }
        public string birim { get; set; }
        public string ad { get; set; }
        public string soyad { get; set; }
        public string resim { get; set; }
        public long dbseri { get; set; }
        public string dbresim { get; set; }
        public string dbadi { get; set; }
        public string ofisno { get; set; }
        public string yetkili { get; set; }
        public string dbmodel { get; set; }
        public string dbmarka { get; set; }
        public string dbkategori { get; set; }
    }


    public class ZimmetRapor
    {
        public long sirano { get; set; }
        public string demirbas { get; set; }
        public string model { get; set; }
        public string marka { get; set; }
        public string kategori { get; set; }
        public DateTime zimmettarih { get; set; }
        public DateTime? iadetarih { get; set; }
        public string aciklama { get; set; }       
    }
}