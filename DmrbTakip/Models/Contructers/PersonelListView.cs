﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DmrbTakip.Models.Contructers
{
    public class PersonelListView
    {
        public int id { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }
    }

    public class OfisListView
    {
        public int id { get; set; }
        public string Ofisno { get; set; }
        public string yetkili { get; set; }
    }

    public class PersonelZimmetView
    {
        public int personel_id { get; set; }
        public string ad { get; set; }
        public string soyad { get; set; }
        public string tcno { get; set; }
        public string resim { get; set; }
        public string birimadi { get; set; }
        public string kat { get; set; }
        public string bina { get; set; }
        public string telefon { get; set; }
        public string eposta { get; set; }
        public string cinsiyet { get; set; }
        public DateTime? dogumtarihi { get; set; }
        public string adres { get; set; }
        public int durum { get; set; }
    }

    public class OfisZimmetView
    {
        public int ofis_id { get; set; }
        public string ofisno { get; set; }       
        public string kat { get; set; }
        public string bina { get; set; }
        public string telefon { get; set; }
        public string eposta { get; set; }       
        public string firma { get; set; }
        public string yetkili { get; set; }
    }
}