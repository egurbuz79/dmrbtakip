﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DmrbTakip.Models.Contructers
{
    public class DemirbasKullanimOranView:IDisposable
    {
        public string DemirbasKAtegori { get; set; }
        public decimal ToplamAdet { get; set; }
        public decimal KullanilanAdet { get; set; }
        public Nullable<decimal> Oran { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}