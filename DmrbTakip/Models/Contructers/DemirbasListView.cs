﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DmrbTakip.Models.Contructers
{
    public class DemirbasView
    {
        public int id { get; set; }
        public string demirbasAdi { get; set; }
        public long serino { get; set; }
    }

    public class DemirbasListView
    {
        public string kategori { get; set; }
        public string marka { get; set; }
        public string model { get; set; }
        public string resim { get; set; }
        public long serino { get; set; }
        public string demirbasadi { get; set; }
        public DateTime alistarihi { get; set; }
        public string zimmet { get; set; }
        public string personel { get; set; }
        public string birim { get; set; }
    }
}