//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DmrbTakip.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Birim
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Birim()
        {
            this.Personels = new HashSet<Personel>();
        }
    
        public int id { get; set; }
        public string birimadi { get; set; }
        public int durum { get; set; }
        public int bina_id { get; set; }
        public int kat_id { get; set; }
    
        public virtual Bina Bina { get; set; }
        public virtual Kat Kat { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Personel> Personels { get; set; }
    }
}
