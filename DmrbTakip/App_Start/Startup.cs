﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Security.Claims;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;

[assembly: OwinStartup(typeof(DmrbTakip.App_Start.Startup))]

namespace DmrbTakip.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            System.Web.Helpers.AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/KimlikDogrulama/GirisYap"),
                CookieSecure = CookieSecureOption.SameAsRequest,
                CookieName = "DmrbTakip",
                SlidingExpiration = true                
            });

        }
    }
}
