using DmrbTakip.Models.Interfaceses;
using DmrbTakip.Models.Methods;
using System;

using Unity;

namespace DmrbTakip
{
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        
        public static IUnityContainer Container => container.Value;
        #endregion        
        public static void RegisterTypes(IUnityContainer container)
        {           
            container.RegisterType<I_Helper, Helper>();
            container.RegisterType<I_Giris, Giris>();           
        }
    }
}